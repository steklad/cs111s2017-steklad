//==========================================
//
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
//David A. Stekla
// CMPSC 111 Spring 2017
// lab 4
// Date: february 9, 2017
//
// Purpose: to create a beautiful and meaningful piece of art using gvim additionally teaches us to embed one program within another
//==========================================

import javax.swing.*;

public class Lab4Display
{
  public static void main(String[] args)
  {
    JFrame window = new JFrame(" David A Stekla ");

    // Add the drawing canvas and do necessary things to
    // make the window appear on the screen!
    window.getContentPane().add(new Lab4());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setSize(600, 400);
  }
}

