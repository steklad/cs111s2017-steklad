//=================================================
//
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
//David A. Stekla
// CMPSC 111 Spring 2017
// lab 4
// Date: february 9, 2017
//
// Purpose: to create a beautiful and meaningful piece of art using gvim additionally teaches us to embed one program within another
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet
{
  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(Graphics page)
  {
	final int WIDTH = 600 ;
	final int HEIGHT = 400 ;

	// make border white
	page.setColor(Color.gray);
	page.fillRect(0,0,WIDTH, HEIGHT);

	// make inner coloration blue
	page.setColor(Color.blue);
	page.fillRect(25,25,550,350); 

	// whale body
	page.setColor(Color.pink);
	page.fillRect(55,100, 250, 200);

	//Whale tail
	page.setColor(Color.pink);
	page.fillRect(250 , 200, 250, 100);

	// upward part of tail
	page.setColor(Color.pink);
	page.fillRect( 440 , 150, 60,100);

	//flipper of tail
	page.setColor(Color.pink);
	page.fillRect(410,100,120,50);

	// whales eye
	page.setColor(Color.darkGray);
	page.fillRect(95, 175, 25, 25);

	// whale mouth
	page.setColor(Color.darkGray);
	page.drawLine(55,250,145,250);
	
	//mouth detailing
	page.drawLine(145,230,145,270);

	//whale eyebrow
	page.drawArc(55, 165, 90,45, 45, 50);




  }
}
