// Honor Code: The work I am submitting is a result of my own thinking and efforts.
//David A. Stekla , Lisa Taapken
// CMPSC 111 Spring 2017
// lab 5
// Date: 2/23/17
//
// Purpose: generate a program that can hide a 10 character message in a 
// 20X20 grid
// 


import java.util.Date;
import java.util.Scanner;
import java.util.Random ;

public class WordHide
{
		public static void main(String[] args)

	{
	//variable declaration
	String message ;
	String mutation1 , mutation2 ;

	//activate scanner tool
	Scanner scan = new Scanner(System.in); 
	Random rand = new Random();
	 
	//beginning of program
	System.out.println("Please enter your 10 character message:");
	message = scan.next();
	mutation1 = message.toUpperCase() ; 
	mutation2 = mutation1.substring(0, 10); // Ensures only 10 character can be input
	System.out.println(mutation2 + " is your message");
	String letter1 = mutation2.substring(0,1);
	String letter2 = mutation2.substring(1,2);
	String letter3 = mutation2.substring(2,3);
	String letter4 = mutation2.substring(3,4);
	String letter5 = mutation2.substring(4,5);
	String letter6 = mutation2.substring(5,6);
	String letter7 = mutation2.substring(6,7);
	String letter8 = mutation2.substring(7,8);
	String letter9 = mutation2.substring(8,9);
	String letter10 = mutation2.substring(9,10); 
	String end1 = "A"; 


	// print out of  word hiding grid below	

 	System.out.println("A C D E F G H I J R Y V J " + letter1 + " I W U C G W"  ); //1
	System.out.println("A E Y C D D G H W C J Y I Q E P H N E Q" ); //2
	System.out.println("A Y " + letter2 + " T Y U L P W K I Y B P W T H Q N I" ); //3
	System.out.println("U R Y T V B E I Q M S I B E O U P S N L"  ); //4 
	System.out.println("P O I N B T E R A W N E N E L W C U I I"  ); //5
	System.out.println("P A I N T B E W W Y C L E I G " + letter6 + " W J N V"  ); //6
	System.out.println("W Z J I T Y V B O W X Z L W I R J " + letter9 + " K D" ); //7
	System.out.println("O P E A V Z U M S E J W W D N E i W E K" ); //8
	System.out.println("G O O D L U C K F I N D I N G I T Q M W" ); //9
	System.out.println("U Y Z D A S L " + letter4 + " E N L E O W N D A L W O"); //10
	System.out.println("Y I T R E Q Z A A A O " + letter8 + " O E H D L E N E"); //11
	System.out.println("P O I Z T R Q W E E N W J C I Y E Q B " + letter3); //12
	System.out.println("W I U P O T R Z A E N V A I Q F E N S K"); //13
	System.out.println("Q X Z C V B E R T W " + letter7 + " H N S K E L F U V" ); //14
	System.out.println("P K L M N O P E R S N C U W L F G N E Q"); //15
	System.out.println("O U Y T " + letter10 + " W N S I B N A O E N L A I W K"); //16
	System.out.println("N B V D R E T Y Z W C H A L E U O P Q N "); //17
	System.out.println( letter5 + " D E W A Q R Z T V Q I D N L S A R M E"); //18
	System.out.println("E R T Y P Z Q V U W D X N A U M L P W E"); //19
	System.out.println("S Q W E R T Y Z A Q N B V S Z J E N B A"); //20

// key to find letters
//   l1     l2    l3      l4    l5      l6      l7     l8      l9     l10
// (1,14) (3,3) (12,20) (10,4) (18,1) (6,16) (14,10) (11,12) (7,18) (15,5) 

	}
}

	
