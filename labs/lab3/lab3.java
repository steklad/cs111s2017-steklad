// Honor Code: The work I am submitting is a result of my own thinking and efforts.
//David A. Stekla
// CMPSC 111 Spring 2017
// lab 3 tip calculator
// Date: feb 2 2017
//
// Purpose: to create a tip calculator that utilizes a scanner in its programming

import java.util.Date; 
import java.util.Scanner;
import java.text.DecimalFormat;

public class lab3
{
	//Main method: program begins here

	public static void main(String[] args)
{

	//name and date
	System.out.println("David A Stekla\nLab 3\n" + new Date() + "\n");

	//programing begins here
	String user; // name of user to greet the person
	double totalcost;  //total cost of the bill
	double percentage;  //percentage you would like to tip
	double split; //number of people to split the bill between
	double tipamount; //amount you should tip based off of percentage
	double billtip; //total cost of the bill + tip
	double per; // final cost per person of the final bill
	int b = 100; 
	// have to divide the percetnage by 100 in order to get a decimal 
	// this term will help create a functioning math program
	// %/100 = decimal to multiply the bill by
	
	Scanner scan = new Scanner(System.in);  // scanner to find values

	Decimalformat fmt = new DecimalFormat("0.##"); // limits number of decimal places to 2
 
	//tip calculator programming

	System.out.println("Please enter your name:");
	user = scan.next();
	System.out.println(user+", welcome to the Tip Caluculator!");
	System.out.print("Please enter the amount of your bill: $");
	totalcost = scan.nextDouble();
	System.out.print("Please enter the percentage that you want to tip: %");
	percentage = scan.nextDouble();
	System.out.println("Your original bill was: $" + totalcost);
	
	tipamount = totalcost * ( percentage / b );
	
	System.out.println("Your tip amount is: $" + tipamount);
	
	billtip = totalcost + tipamount;

	System.out.println("Your total bill is: $" + billtip);

	System.out.println("How many people will be spliting the bill?");
	split = scan.nextDouble();

	per = billtip / split;


	System.out.println("Each person should pay: $" + fmt.format(per));
	System.out.println("Have a nice day! Thank you for using our service!");

	
}
}

