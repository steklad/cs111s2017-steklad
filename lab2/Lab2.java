// Honor Code: The work I am submitting is a result of my own thinking and efforts.
//David A. Stekla
// CMPSC 111 Spring 2017
// Lab 
// Date: Jan 26 2017
// Purpose: create a program that will use van der waal parameters and temperature to find B2T


import java.util.Date; 

public class Lab2
{
	//Main method: program begins here

	public static void main(String[] args)
{

	//name and date printout
	System.out.println("David A Stekla\nLab 2\n" + new Date() + "\n");

	//Variables listed below
	double b = 4; // first van der waal parameter
	double a = 3; // second van der waal parameter
	double R = 8.314; // constant R
	double T = 298; // temp in K
	double B2T;

	//computation, equation below
	B2T = b - (a / (R * T));

	// output of answer
	System.out.println("First van der waal parameter: " + b);
	System.out.println("Second van der waal parameter: " + a);
	System.out.println("Constant R: " + R);
	System.out.println("Temperature in Kelvin: " + T);
	System.out.println("B2T value found to be: " + B2T);

}

	}

	


