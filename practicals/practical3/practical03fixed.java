//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// David Nadel 
// CMPSC 111 Spring 2017
// Practical 3, February, 2017
// 
//*************************************
import java.util.Date; 
import java.util.Scanner;

public class Practical03
{
public static void main(String[] args)
{
// Label output with name and date:
System.out.println("David Nadel\n Lab #3 "+ new Date() );

// Variable dictionary:
//[Declare variables and use comments to explain their meanings]
String name;
float amount;
float tip=0.0f;
float percentage;
float totalbill;
float people;
float share;

Scanner scan = new Scanner(System.in);

System.out.println("Enter your username: ");
name = scan.nextLine();
System.out.println(name +", Welcome to the Tip Calculator!");
System.out.print("Enter the total of your bill: $"); 
amount = scan.nextFloat();
System.out.println("Enter the percentage that you want to tip: ");
percentage = scan.nextFloat();
tip = (percentage/100) * amount;
totalbill = tip + amount;
System.out.println("Your orginal bill was $"+ amount);
System.out.println("Your tip amount is $"+ tip);
System.out.println("Your total bill is $"+ totalbill);
System.out.println("How many people will be splitting this bill?");
people = scan.nextFloat();
share = totalbill / people ;
System.out.println("Each person should pay $"+ share);
System.out.println ("Have a nice day! Thank you for ordering at our restaurant");
}
}

