// Honor Code: The work I am submitting is a result of my own thinking and efforts.
//David A. Stekla
// CMPSC 111 Spring 2017
// practical 5
// Date: 2/24/17
//
// Purpose: create a mad libs program that will create a small story

import java.util.Date; 
import java.util.Scanner ;

public class practical5
{
	//Main method: program begins here

	public static void main(String[] args)
{

	//name and date
	System.out.println("David A Stekla\npractical5 \n" + new Date() + "\n");

	//defining tools
	Scanner scan = new Scanner(System.in) ; 

	// defining variables
	String verb , name , adjective ;
	int dtd , pkp ;
	double death ;
	int q = 7 ;

	// program inputs begin here for main character
	System.out.println("Please enter a name: ");
	name = scan.next();
	System.out.println("Please enter a verb: ");
	verb = scan.next();
	System.out.println("Please enter a adjective: ");
	adjective = scan.next();
	System.out.println("Please enter the number of Heroes: ");
	dtd = scan.nextInt() ;
	System.out.println("Please enter the number of Villians: ");
	pkp = scan.nextInt();
	
	death = (dtd * pkp) / q ;

	// story programming

	System.out.println("Once upon a time a young " + adjective + " by the name of " + name + ", decided the Delts and Phi Psis should settle their differences in beautiful and brutal " + verb );
	
	System.out.println("Everyone heard of this contest, and the campus flocked to witness the contest. Legend tells that " + dtd + " honorable Delts, and " + pkp + " meh Phi Psi's participated.");

	System.out.println("The Brutal contest took place at high noon at Brooks Walk. Both sides eager for victory charged and so it began. ");

	System.out.println("After 8 long days and nights the contest concluded, and once the dust settle " + death + " Fijis lay dead, both sides realized their mistakes and decided to never fued again.");

	System.out.println("THE END");


}
}

