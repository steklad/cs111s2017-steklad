//David Stekla
//practical 6
// 3/31/17
//
import java.util.Date ;
import java.util.Scanner ;
import java.util.Random ;

public class practical6
{
	public static void main(String[] args)
{
	System.out.println("David A Stekla \n practical 6 \n" + new Date()) ;

	Scanner scan = new Scanner(System.in) ;
	Random rand = new Random() ;
	
	// sets to find a random number max of fifty min of 1
	int realnum = rand.nextInt(100) + 1;
	int guess  ;
	int count = 0 ;
	// Prompts number to input
	System.out.println("Please input a integer between 1-100 and try to guess what the real number is");
	System.out.println("Input integer here: ");
	guess = scan.nextInt();

	//while loop about guess
	while (guess != realnum) 
	{ 
	count++ ;
	System.out.println("Oops thats not quite right!");
	
	if (guess < realnum)
	{
	System.out.println("Your number was too low try again: " );
	guess = scan.nextInt();
	}
	if (guess > realnum)
	{
	System.out.println("Your number was too high try again: ");
	guess = scan.nextInt();
	}
	}

	System.out.println("Congradulations you successfully guessed the number " + realnum);

	System.out.println("It only took you " + count + " tries!");
}
}
